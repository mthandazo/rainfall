#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int         main(int ac, char **av)
{
    char    *ptr;

    if (ac < 2)
    {
        printf("Usage: %s <env variable> <program name>\n", av[0]);
        exit(0);
    }
    ptr = getenv(av[1]);
    ptr += (strlen(av[0]) - strlen(av[2])) * 2;
    printf("%s is located at %p\n", av[1], ptr);
    return 0;
}
